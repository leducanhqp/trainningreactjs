import React, { Component } from 'react';
import Header from './components/Header';
import Product from './components/Product'; 
import Table from './components/Table'; 
import Gmail from './components/Gmail'; 
import './App.css';

class App extends Component {
    render() {
        var products = [
            {
                id : 1,
                image : "https://cdn.tgdd.vn/Products/Images/42/69783/iphone-6-plus-16gb-14-300x300.jpg",
                name : 'Iphone 6 Plus 16GB Gold',
                description : 'The iPhone 6 Plus breaks a lot of the rules and defies a lot of the logic that Apple has stuck to for a number of years.',
                price : '5.000 USD'
            },
            {
                id : 2,
                image : "https://cdn.tgdd.vn/Products/Images/42/69783/iphone-6-plus-16gb-14-300x300.jpg",
                name : 'Iphone 6 Plus 16GB Gray',
                description : 'The iPhone 6 Plus breaks a lot of the rules and defies a lot of the logic that Apple has stuck to for a number of years.',
                price : '5.000 USD'
            },
            {
                id : 3,
                image : "https://cdn.tgdd.vn/Products/Images/42/69783/iphone-6-plus-16gb-14-300x300.jpg",
                name : 'Iphone 6 Plus 16GB Rose',
                description : 'The iPhone 6 Plus breaks a lot of the rules and defies a lot of the logic that Apple has stuck to for a number of years.',
                price : '5.000 USD'
            },
        ];

    let elements = products.map((product, index) => {
        return <Product 
                    key         = { product.id }
                    image       = { product.image }
                    name        = { product.name }
                    description = { product.description}
                    price       = { product.price }                     
                />
    })

        return (
            <div>
                <Header />
                <h1 className="text-center">PROPS</h1>  
                <div className="container">
                    <div className="row">
                        <div className="product col-xs-12 col-md-12 col-lg-12">
                            { elements }
                        </div>
                    </div>
                </div>
                <h1 className="text-center">STATE</h1>
                <Table />
                <h1 className="text-center">FORM</h1>
                <Gmail />
            </div>
        )
    }
}

export default App;