import React, { Component } from 'react';

class Gmail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fullName : ''
        };
        this.handleChange       = this.handleChange.bind(this); 
        this.handleSubmitForm   = this.handleSubmitForm.bind(this);
    }

    handleSubmitForm(event) {
        alert('Hello ' + this.state.fullName);
        event.preventDefault();
    }

    handleChange(event) {
        var value = event.target.value;
        this.setState ({
            fullName : value
        });
    }
    render() {
        return (
            <form onSubmit={this.handleSubmitForm}>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput1">Name : 
                        <input 
                            type="text" 
                            value = {this.state.fullName}
                            onChange = {this.handleChange}
                        />
                    </label>
                    <input type="submit" value="Submit" />
                    <p>{this.state.fullName}</p>  
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlTextarea1">Example textarea</label>
                    <textarea className="form-control" id="exampleFormControlTextarea1" rows={3} defaultValue={""} />
                </div>
            </form>
        )
    }
}

export default Gmail;