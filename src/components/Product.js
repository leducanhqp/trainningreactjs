import React, { Component } from 'react';

class Product extends Component {

    onClick =  () =>  {
        alert( this.props.name );
    }

    render() {
        return (
            <div className="col-lg-4 col-md-4 col-sm-4">
                <div className="card" style={{width: '18rem'}}>
                    <div className="card-body">
                        <h5 className="card-title">
                            { this.props.name }
                        </h5>
                        <p className="card-text">
                            { this.props.description }
                        </p>
                        <h6 className="card-text">
                            { this.props.price }
                        </h6>
                        <button type="button" className="btn btn-outline-secondary" 
                        onClick= {this.onClick}>
                            Click Buy
                        </button>
                    </div>
                </div>
            </div>
            
        );
    }
}

export default Product;  