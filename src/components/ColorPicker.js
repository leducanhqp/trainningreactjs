import React, { Component } from 'react';
import './ColorPicker.css';

class ColorPicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            colors : [ 'red' , 'green' , 'black' ]
        }
    }

    showColor(color) {
        console.log(color);
    }

    render() {

        let elementColor = this.state.colors.map((color, index) => {
            return  <span key = {index} style= { this.showColor(color)}  >

                    </span>
        });
        return (
            <div>
                <div className="card bg-light mb-3 " style={{maxWidth: '100%'}}>
                    <div className="card-header">Color Picker</div>
                    <div className="card-body"> 
                        {elementColor}
                    </div>
                </div>
            </div>
            
        );
    }
}

export default ColorPicker;  