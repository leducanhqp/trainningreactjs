import React, { Component } from 'react';

class Table extends Component {
    constructor(props) {
        super();
        this.state = {
            products : [
                {
                    id : 1,
                    name : 'Iphone 6 Plus 16GB Gray',
                    description : 'The iPhone 6 Plus breaks a lot of the rules and defies a lot of the logic that Apple has stuck to for a number of years.',
                    price : '5.000 USD'
                },
                {
                    id : 2,
                    name : 'Iphone 6 Plus 16GB Gray',
                    description : 'The iPhone 6 Plus breaks a lot of the rules and defies a lot of the logic that Apple has stuck to for a number of years.',
                    price : '5.000 USD'
                },
                {
                    id : 3,
                    name : 'Iphone 6 Plus 16GB Gray',
                    description : 'The iPhone 6 Plus breaks a lot of the rules and defies a lot of the logic that Apple has stuck to for a number of years.',
                    price : '5.000 USD'
                }
            ]
        };
    }

    

    render() {

        let element = this.state.products.map((product, index) => {
            return  <tr key={index}>
                        <th scope="row">{ index } </th>
                        <td> {product.name} </td>
                        <td> {product.description} </td>
                        <td> {product.price} </td>
                    </tr>
        });

        return (
            <div>
                <table className="table">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">STT</th>
                            <th scope="col">Name</th>
                            <th scope="col">description</th>
                            <th scope="col">price</th>
                        </tr>
                    </thead>
                    <tbody>
                        { element }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Table;